import { Component, OnInit, EventEmitter, Input, Output  } from '@angular/core';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.css']
})
export class TwoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  countForOne = 0;
  @Input('countToTwo') countForThis = 0;
  @Output() detectChange = new EventEmitter();

  changeOne() {
    this.countForOne = this.countForOne + 1;
    this.detectChange.emit(this.countForOne);
  }

}
