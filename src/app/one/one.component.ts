import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {
  ngOnInit(): void {
  }
  countForTwo = 0;
  @Input('countToOne') countForOne = 0;
  @Output() detectChange = new EventEmitter();

  changeTwo() {
    this.countForTwo = this.countForTwo + 1;
    this.detectChange.emit(this.countForTwo);
  }

}
