import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  countTwo = 0;
  countOne = 0;

  obtainFirstValue(value: number) {
    this.countTwo = value;
  }




  obtainSecondValue(value: number) {
    this.countOne = value;
  }
}
